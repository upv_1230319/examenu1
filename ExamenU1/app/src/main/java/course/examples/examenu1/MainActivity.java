package course.examples.examenu1;

import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;


public class MainActivity extends ActionBarActivity {
    public double Base,Altura;
    public EditText edit_Base, edit_Altura;
    public TextView Resultado;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        edit_Base = (EditText) findViewById(R.id.editText1);
        edit_Altura = (EditText) findViewById(R.id.editText2);
        Resultado = (TextView) findViewById(R.id.text_View3);
    }

    public void Calcular(View view){
        Base = Double.parseDouble(edit_Base.getText().toString());
        Altura = Double.parseDouble(edit_Altura.getText().toString());
        Resultado.setText(Double.toString(Base * Altura));
    }
}
